//
//  ViewModel.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "ViewModel.h"
#import "Model.h"
#import "DataService.h"

@implementation ViewModel

-(instancetype)initWithService:(DataService *)dataService{
    self = [super init];
    if (!self) return nil;
    _zones = [NSMutableArray new];
    _dataService = dataService;
    [self retrieveData];
    return self;
}

//Use data service to get data from local JSON
-(void)retrieveData{
    NSDictionary* data = [_dataService parseJSONFromLocalFile];
    
    _center = [[PointModel alloc] initWithString:[data valueForKey:@"current_location"]];
    
    NSArray *zonesStringArray = [[data objectForKey:@"location_data"] valueForKey:@"zones"];
    
    for (NSString* str in zonesStringArray){
        ZoneModel *zone = [[ZoneModel alloc] initWithString:str];
        [_zones addObject:zone];
    }
    
}

//Detect if annotation is in polygon or not
-(BOOL)isPoint:(CLLocationCoordinate2D)coord inOverlay:(MKPolygon*)polygon{
    MKMapPoint mapPoint = MKMapPointForCoordinate(coord);
    CGPoint cgpPoint = CGPointMake(mapPoint.x, mapPoint.y);
    
    CGMutablePathRef pathRef = [self pathRefForPolygon:polygon];
    
    BOOL inside =  CGPathContainsPoint(pathRef, NULL, cgpPoint, FALSE);
    CGPathRelease(pathRef);
    return inside;
}

//Construct polygon path
-(CGMutablePathRef)pathRefForPolygon:(MKPolygon*)polygon{
    CGMutablePathRef pathRef = CGPathCreateMutable();
    MKMapPoint* points =  polygon.points;
    
    for (int i=0; i<polygon.pointCount; i++){
        MKMapPoint pt = points[i];
        if ( i== 0){
            CGPathMoveToPoint(pathRef, NULL, pt.x, pt.y);
        } else {
            CGPathAddLineToPoint(pathRef, NULL, pt.x, pt.y);
        }
    }
    
    return pathRef;
}


@end
