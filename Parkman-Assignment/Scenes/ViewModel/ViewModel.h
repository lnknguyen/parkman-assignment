//
//  ViewModel.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//



#import <MapKit/MapKit.h>
@class DataService;

@class PointModel;

@interface ViewModel : NSObject
@property (nonatomic, readonly) PointModel* center;
@property (nonatomic, readonly) NSMutableArray* zones;

//Dependency injection here for later services injection
@property (nonatomic,readonly) DataService *dataService;

-(instancetype)initWithService:(DataService*)dataService;
-(void)retrieveData;

//These functions should be seperated to a category or helper file
-(CGMutablePathRef)pathRefForPolygon:(MKPolygon*)polygon;
-(BOOL)isPoint:(CLLocationCoordinate2D)coord inOverlay:(MKPolygon*)polygon;

@end
