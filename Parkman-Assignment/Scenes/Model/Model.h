//
//  Model.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#ifndef Model_h
#define Model_h
#import <MapKit/MapKit.h>
//MARK: - Point model
@interface PointModel : NSObject

@property (nonatomic,readonly) float latitude;
@property (nonatomic,readonly) float longitude;
-(instancetype)initWithString:(NSString*)data;
-(MKMapPoint)toMKMapPoint;
-(CLLocationCoordinate2D)toCLLLocationCoordinate2d;

@end

//MARK: - Polygon model
@interface PolygonModel : NSObject

@property (nonatomic,readonly) NSMutableArray* pointArray;
-(instancetype)initWithString:(NSString*)data;
-(MKPolygon*)toMKPolygon;
@end



//MARK: - Zone model
@interface ZoneModel : NSObject

@property (nonatomic,readonly) int zoneId;
@property (nonatomic,readonly) int providerId;

@property (nonatomic,readonly) float price;
@property (nonatomic,readonly) float duration;

@property (nonatomic,readonly) NSString *name;
@property (nonatomic,readonly) NSString *contactEmail;
@property (nonatomic,readonly) NSString *providerName;

@property (nonatomic,readonly) BOOL paymentIsAllowed;

@property (nonatomic,readonly) MKMapPoint center;
@property (nonatomic,readonly) MKPolygon *polygon;


-(instancetype)initWithString:(NSString*)data;

@end





#endif /* Model_h */
