//
//  Model.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "Model.h"
#import <MapKit/MapKit.h>
//MARK: Zone model
@implementation ZoneModel

-(instancetype)initWithString:(NSString *)data {
    self = [super init];
    if (!self) return nil;
    
    PolygonModel *plModel = [[PolygonModel alloc] initWithString:[data valueForKey:@"polygon"]];
    PointModel *ptModel = [[PointModel alloc] initWithString:[data valueForKey:@"point"]];
    
    _center = [ptModel toMKMapPoint];
    _polygon = [plModel toMKPolygon];
    
    //Static data -> catching error is redundant
    //Usually an object mapper library will handle the error catch
    
    _name = [data valueForKey:@"name"];
    _zoneId = [[data valueForKey:@"id"] intValue];
    _paymentIsAllowed = [[data valueForKey:@"payment_is_allowed"] boolValue];
    _providerId = [[data valueForKey:@"provider_id"] intValue];
    _providerName = [data valueForKey:@"provider_name"];
    _price = [[data valueForKey:@"service_price"] floatValue];
    _contactEmail = [data valueForKey:@"contact_email"];
    _duration = [[data valueForKey:@"max_duration"] floatValue];
    return self;
}

@end

//MARK: Point model
@implementation PointModel

-(instancetype)initWithString:(NSString *)data{
    self = [super init];
    if (!self) return nil;
    
    //Trim trailing and ending whitespace
    data = [data stringByTrimmingCharactersInSet:[NSCharacterSet whitespaceCharacterSet]];
    //Split array into 2 (lat & lon)
    NSArray *point = [data componentsSeparatedByString:@" "];
    //Map to coordinate
    _latitude = [point[0] floatValue];
    _longitude = [point[1] floatValue];
    return self;
}

-(MKMapPoint)toMKMapPoint{
    return MKMapPointMake(_latitude, _longitude);
}

- (CLLocationCoordinate2D)toCLLLocationCoordinate2d{
    return CLLocationCoordinate2DMake(_latitude, _longitude);
}
@end

//MARK: Polygon model
@implementation PolygonModel

-(instancetype)initWithString:(NSString *)data{
    _pointArray = [NSMutableArray new];
    //Split to multiple point's type strings
    NSArray *pointStringArray = [data componentsSeparatedByString:@","];
    //Iterate to do the mapping
    for (NSString* str in pointStringArray){
        PointModel *point = [[PointModel alloc] initWithString:str];
        [_pointArray addObject:point];
    }
    return self;
}

-(MKPolygon*)toMKPolygon{
    NSUInteger count = _pointArray.count;
    //Create C style array of coordinates
    CLLocationCoordinate2D *points = malloc(sizeof(CLLocationCoordinate2D) * count);
    for (int i =0; i< count; i++){
        points[i] = [_pointArray[i] toCLLLocationCoordinate2d];
    }
    MKPolygon *polygon = [MKPolygon polygonWithCoordinates:points count:count];
    //Free array after use
    free(points);
    return polygon;
}

@end
