//
//  ViewController.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import "ViewController.h"
#import "DataService.h"
#import "Model.h"
#import "ViewModel.h"
#import "MKAnnotation.h"
#import "CalloutView.h"
#import "AnnotationView.h"
@interface ViewController ()
@property (nonatomic) IBOutlet CalloutView* calloutView;
@property (nonatomic) AnnotationView* annotationView;
@property (nonatomic) ZoneModel* occupiedZone;
@end

@implementation ViewController

-(void) bindViewModel:(ViewModel *)viewModel{
    _viewModel = viewModel;
}
- (void)viewDidLoad {
    [super viewDidLoad];
    
    [_mapView setDelegate:self];
    [self initComponentViews];
    [self configureMapView];
    
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

//Create components view from xib
-(void)initComponentViews{
    _calloutView = [[[NSBundle mainBundle] loadNibNamed:@"CalloutView" owner:self options:nil] objectAtIndex:0];
    [_calloutView.parkhereButton addTarget:self action:@selector(parkhereBtnTapped) forControlEvents:UIControlEventTouchUpInside];
    
    _annotationView = [[[NSBundle mainBundle] loadNibNamed:@"AnnotationView" owner:self options:nil] objectAtIndex:0];
}

//Handler for Park button
-(void)parkhereBtnTapped{
    //Simply alert the user about their parking zone
    NSString *message = [NSString stringWithFormat:@"Name: %@ \r\n Provider: %@ \r\n Price: %.2f€ \r\n Max duration: %.2f",
                         _occupiedZone.name,_occupiedZone.providerName,_occupiedZone.price, _occupiedZone.duration];
    UIAlertController *alert = [UIAlertController alertControllerWithTitle:@"Your parking zone" message:message preferredStyle:UIAlertControllerStyleAlert];
    
    UIAlertAction* okButton = [UIAlertAction
                                actionWithTitle:@"OK"
                                style:UIAlertActionStyleDefault
                               handler:nil];
    
    [alert addAction:okButton];
    [self presentViewController:alert animated:TRUE completion:nil];
    
}
//Mapview configuration
-(void)configureMapView {
    
    [_mapView setMapType:MKMapTypeStandard];
    [_mapView setZoomEnabled:TRUE];
    [_mapView setPitchEnabled:TRUE];
    
    
    //Setup visible region
    
    //Setup center point
    CLLocationCoordinate2D center =  CLLocationCoordinate2DMake(_viewModel.center.latitude, _viewModel.center.longitude);
    
    //Visible region , span of 1800
    MKCoordinateRegion region = [_mapView regionThatFits:MKCoordinateRegionMakeWithDistance(center, 1800, 1800)];
    [_mapView setRegion:region animated:TRUE];
    
    //Add overlays
    for (ZoneModel* zone in _viewModel.zones){
        [_mapView addOverlay:zone.polygon];
    }
    
    //Add an annotation in map's center
    CLLocationCoordinate2D centerCoordinate = [_viewModel.center toCLLLocationCoordinate2d];
    _centerAnnotation = [[MKAnnotation alloc] initWithCoordinate:centerCoordinate];
    [_mapView addAnnotation:_centerAnnotation];
}

//MARK: - Delegate for mkmapview

//Setup view for annotiations
-(MKAnnotationView *)mapView:(MKMapView *)mapView viewForAnnotation:(id<MKAnnotation>)annotation{
    
    MKAnnotationView *view = nil;
    if ([annotation isKindOfClass:[MKAnnotation class]]){
        view = (MKAnnotationView*)[mapView dequeueReusableAnnotationViewWithIdentifier:@"view"];
        if ( view == nil ){
            view = [[MKAnnotationView alloc] initWithAnnotation:annotation reuseIdentifier:@"view"];
        }
        //set origin
        _annotationView.frame = CGRectMake(view.frame.origin.x, view.frame.origin.y,_annotationView.frame.size.width,_annotationView.frame.size.height);
        view.frame = _annotationView.frame;
        [view addSubview:_annotationView];

        //Offset center so that the pin will point to correct coordinate
        view.centerOffset = CGPointMake(0.0, -_annotationView.frame.size.height/2 + 10);
        view.annotation = annotation;
        view.canShowCallout = TRUE;
        
        return view;
    }
    return nil;
}

-(void)mapView:(MKMapView *)mapView didSelectAnnotationView:(MKAnnotationView *)view{
    //Pop up call out view
    CGRect calloutFrame = [_calloutView frame];
    calloutFrame.origin = CGPointMake(-calloutFrame.size.width/2 + 35, -calloutFrame.size.height);
    _calloutView.frame = calloutFrame;
  
    //Setup proper constraints
    NSLayoutConstraint* widthConstraint = [NSLayoutConstraint constraintWithItem:_calloutView attribute:NSLayoutAttributeWidth relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:_calloutView.frame.size.width];
    NSLayoutConstraint* heightConstraint = [NSLayoutConstraint constraintWithItem:_calloutView attribute:NSLayoutAttributeHeight relatedBy:NSLayoutRelationEqual toItem:nil attribute:NSLayoutAttributeNotAnAttribute multiplier:1 constant:_calloutView.frame.size.height];
    [_calloutView addConstraint:widthConstraint];
    [_calloutView addConstraint:heightConstraint];
    
    [view setDetailCalloutAccessoryView:_calloutView];
}

-(void)mapView:(MKMapView *)mapView didDeselectAnnotationView:(MKAnnotationView *)view{
    //Empty detailCallout
    [view setDetailCalloutAccessoryView:nil];
}


//Render overlay, polygon in this case
-(MKOverlayRenderer*)mapView:(MKMapView *)mapView rendererForOverlay:(id<MKOverlay>)overlay{
    if ([overlay isKindOfClass:MKPolygon.class]){
        MKPolygon *polygon = (MKPolygon*)overlay;
        MKPolygonRenderer *renderer = [[MKPolygonRenderer alloc] initWithPolygon:polygon];
        
        //Attributes
        [renderer setStrokeColor:[UIColor redColor]];
        [renderer setLineWidth:2.5];
        [renderer setAlpha:0.8];
        
        //Find overlay from zone
        for (ZoneModel* zone in _viewModel.zones){
            if (zone.polygon == polygon){
                //Background for allow payment or not
                if (zone.paymentIsAllowed){
                    [renderer setFillColor:[UIColor greenColor]];
                } else {
                    [renderer setFillColor:[UIColor grayColor]];
                }
            }
        }
        return renderer;
    }
    return nil;
}

//Handle pin while dragging
-(void)mapView:(MKMapView *)mapView regionWillChangeAnimated:(BOOL)animated{
    //Deselect pin to have a nil detailed accessory view
    [_mapView deselectAnnotation:_centerAnnotation animated:FALSE];
    
    //Change background color back
    MKPolygonRenderer* renderer =(MKPolygonRenderer*)[_mapView rendererForOverlay:_occupiedZone.polygon];
    _occupiedZone.paymentIsAllowed ? [renderer setFillColor:[UIColor greenColor]] : [renderer setFillColor:[UIColor grayColor]];
    
    [renderer setNeedsDisplay];
    [_annotationView.priceLabel setText:@""];
    
    //Hide pin
    [[mapView viewForAnnotation:_centerAnnotation] setHidden:TRUE];
    
    //Simulate hiding annotation
    [[mapView viewForAnnotation:_centerAnnotation] setAlpha:0.0];
    
}

//Finish dragging, do a check if annotation is inside a polygon
-(void)mapView:(MKMapView *)mapView regionDidChangeAnimated:(BOOL)animated{
    
    CLLocationCoordinate2D centerCoord = [mapView centerCoordinate];
    [_centerAnnotation setCoordinate:centerCoord];
    //Enable annotation again
    [UIView animateWithDuration:0.2f
                     animations:^{
                         [[mapView viewForAnnotation:_centerAnnotation] setHidden:FALSE];
                         [[mapView viewForAnnotation:_centerAnnotation] setAlpha:1.0];
                     }];
    
    //Find overlay from zone
    for (ZoneModel* zone in _viewModel.zones){
        //Check if annotation is inside polygon
        if ([_viewModel isPoint:centerCoord inOverlay:zone.polygon] == TRUE){
            //Save occupied zone
            _occupiedZone = zone;
            
            //Title must be set in order to trigger detailed accessory
            [_centerAnnotation setTitle:@"Parking"];
            
            //Select annotation will trigger detail accessory to pop out
            [mapView selectAnnotation:_centerAnnotation animated:FALSE];
            
            //Publish info into callout
            [_calloutView setupViewWithName:zone.name email:zone.contactEmail provider:zone.providerName];
            
            //Set price tag
            [_annotationView.priceLabel setText:[NSString stringWithFormat:@"%.2f€",zone.price]];
            
            //Change background color for occupied space
            MKPolygonRenderer* renderer =(MKPolygonRenderer*)[_mapView rendererForOverlay:zone.polygon];
            [renderer setFillColor:[UIColor yellowColor]];
            [renderer setNeedsDisplay];
        }
    }
}

@end
