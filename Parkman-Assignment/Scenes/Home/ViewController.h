//
//  ViewController.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MapKit/MapKit.h>

@class ViewModel;
@class MKAnnotation;

@interface ViewController : UIViewController <MKMapViewDelegate>

@property (weak, nonatomic) IBOutlet MKMapView *mapView;

@property (nonatomic, readonly) MKAnnotation *centerAnnotation;
@property (nonatomic, readonly) ViewModel *viewModel;
-(void) bindViewModel:(ViewModel*)viewModel;


@end

