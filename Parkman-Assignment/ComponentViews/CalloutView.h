//
//  CalloutView.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#ifndef CalloutView_h
#define CalloutView_h
#import <UIKit/UIKit.h>

@interface CalloutView : UIView
@property (weak, nonatomic) IBOutlet UILabel *nameLabel;
@property (weak, nonatomic) IBOutlet UILabel *contactEmail;

@property (weak, nonatomic) IBOutlet UILabel *providerLabel;
@property (weak, nonatomic) IBOutlet UIButton *parkhereButton;

-(void)setupViewWithName:(NSString*)name email:(NSString*)email provider:(NSString*)provider;
@end

#endif /* CalloutView_h */
