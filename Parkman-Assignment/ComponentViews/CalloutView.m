//
//  CalloutView.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "CalloutView.h"

@implementation CalloutView

-(void)setupViewWithName:(NSString*)name email:(NSString *)email  provider:(NSString*)provider{
    [_nameLabel setText:name];
    [_contactEmail setText:email];
    [_providerLabel setText:provider];
}

@end
