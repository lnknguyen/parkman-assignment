//
//  MapAnnotation.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "MKAnnotation.h"
#import <MapKit/MapKit.h>

@implementation MKAnnotation

-(instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate{
    self = [super init];
    if (!self) return NULL;
    
    _coordinate = coordinate;
    return self;
}

-(void)setCoordinate:(CLLocationCoordinate2D)newCoordinate{
    _coordinate = newCoordinate;
}
@end
