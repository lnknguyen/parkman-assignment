//
//  AnnotationView.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/21/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#ifndef AnnotationView_h
#define AnnotationView_h
#import <UIKit/UIKit.h>

@interface AnnotationView : UIView
@property (weak, nonatomic) IBOutlet UIImageView *backgroundImageView;
@property (weak, nonatomic) IBOutlet UILabel *priceLabel;

@end


#endif /* AnnotationView_h */
