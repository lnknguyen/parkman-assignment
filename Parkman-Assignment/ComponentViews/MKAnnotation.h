//
//  MapAnnotation.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/20/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#ifndef MapAnnotation_h
#define MapAnnotation_h

#import <MapKit/MapKit.h>


@interface MKAnnotation : NSObject <MKAnnotation>

@property (nonatomic, readonly) CLLocationCoordinate2D coordinate;
@property (nonatomic, copy) NSString* title;
@property (nonatomic, copy) NSString* subtitle;
- (instancetype)initWithCoordinate:(CLLocationCoordinate2D)coordinate;

@end

#endif /* MapAnnotation_h */
