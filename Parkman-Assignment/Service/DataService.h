//
//  DataService.h
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//


#ifndef DataService_h
#define DataService_h

@interface DataService : NSObject
-(NSDictionary*)parseJSONFromLocalFile;

@end
#endif /* DataService_h */
