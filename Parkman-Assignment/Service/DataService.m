//
//  DataService.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "DataService.h"
#import "Model.h"
@implementation DataService

-(NSDictionary*)parseJSONFromLocalFile{
    //Get file path
    NSString *filePath = [[NSBundle mainBundle] pathForResource:@"response" ofType:@"json"];
    
    //Get raw data from file
    NSString *data = [[NSString alloc] initWithContentsOfFile:filePath encoding:NSUTF8StringEncoding error:NULL];
    
    //Parse raw data to json
    NSDictionary* json = [NSJSONSerialization JSONObjectWithData:[data dataUsingEncoding:NSUTF8StringEncoding] options:0 error:NULL];
    
    return json;
}

@end
