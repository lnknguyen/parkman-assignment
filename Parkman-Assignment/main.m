//
//  main.m
//  Parkman-Assignment
//
//  Created by Nguyen Luong on 11/19/16.
//  Copyright © 2016 Nguyen Luong. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "AppDelegate.h"

int main(int argc, char * argv[]) {
    @autoreleasepool {
        return UIApplicationMain(argc, argv, nil, NSStringFromClass([AppDelegate class]));
    }
}
